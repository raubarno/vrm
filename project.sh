#!/bin/bash

function build() {
  if [ -f ./meson.build ]; then
    if [ ! -d ./target ]; then
      mkdir target
    fi
    meson setup --reconfigure target
    (cd target && meson compile)
  else
    echo "Error: not in a project root (missing 'meson.build')"
  fi
}

function run() {
  if [ -f ./meson.build ]; then
    target/vrm
  else 
    echo "Error: not in a project root (missing 'meson.build')"
  fi
}

if [ "$1" == "build" ]; then
  build
elif [ "$1" == "run" ]; then
  run
else 
  if [ "$0" != "$SHELL" ]; then 
    echo "USAGE: $0 [build | run]"
    echo "Note: you can source this script with 'source $0'"
  fi
fi
