// Copyright (c) 2023 Arnoldas Rauba
//
//    This program is free software: you can redistribute it
//    and/or modify it under the terms of the
//    GNU General Public License as published by the
//    Free Software Foundation, either version 3 of the License,
//    or (at your option) any later version.
//
//    This program is distributed in the hope that it will be
//    useful, but WITHOUT ANY WARRANTY; without even the implied
//    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//    PURPOSE.
//
//    See the GNU General Public License for more details.
//    You should have received a copy of the
//    GNU General Public License along with this program.
//    If not, see <http://www.gnu.org/licenses/>.

#ifndef H_BYTEVEC
#define H_BYTEVEC
#include "lang.h"
#include "slice.h"
#include <stddef.h>

struct bytevec_t {
  size_t len;
  size_t cap;
  u8 *data;
};

struct bytevec_t bytevec_new(size_t len);

void bytevec_push_back(struct bytevec_t *self, u8 *data, size_t len);

void bytevec_push_back_w0(struct bytevec_t *self, u8 *data, size_t len);

void bytevec_pop_front(struct bytevec_t *self, size_t len);

struct byteslice_t bytevec_as_slice(struct bytevec_t *self, size_t from,
                                    size_t len);

struct bytevec_t bytevec_clone(struct byteslice_t *from);

struct bytevec_t bytevec_clone_w0(struct byteslice_t *from);

void bytevec_clear(struct bytevec_t *self);

void bytevec_drop(struct bytevec_t self);
#endif
