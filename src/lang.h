// Copyright (c) 2023 Arnoldas Rauba
//
//    This program is free software: you can redistribute it
//    and/or modify it under the terms of the
//    GNU General Public License as published by the
//    Free Software Foundation, either version 3 of the License,
//    or (at your option) any later version.
//
//    This program is distributed in the hope that it will be
//    useful, but WITHOUT ANY WARRANTY; without even the implied
//    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//    PURPOSE.
//
//    See the GNU General Public License for more details.
//    You should have received a copy of the
//    GNU General Public License along with this program.
//    If not, see <http://www.gnu.org/licenses/>.

#ifndef H_LANG
#define H_LANG
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef char i8;
typedef short i16;
typedef int i32;
typedef long long i64;
typedef float f32;
typedef u8 ptr;

enum ins_t {
  I_SET1,
  I_SET2,
  I_SET4,
  I_SET8,
  I_REFSET1,
  I_REFSET2,
  I_REFSET4,
  I_REFSET8,
  I_REFREF1,
  I_REFREF2,
  I_REFREF4,
  I_REFREF8,
  I_INC1,
  I_INC2,
  I_INC4,
  I_INC8,
  I_DEC1,
  I_DEC2,
  I_DEC4,
  I_DEC8,
  I_IFZ1,
  I_IFZ2,
  I_IFZ4,
  I_IFZ8,
  I_IFP1,
  I_IFP2,
  I_IFP4,
  I_IFP8,
  I_IFPZ1,
  I_IFPZ2,
  I_IFPZ4,
  I_IFPZ8,
  I_IMUL1,
  I_IMUL2,
  I_IMUL4,
  I_IMUL8,
  I_UMUL1,
  I_UMUL2,
  I_UMUL4,
  I_UMUL8,
  I_IDIV1,
  I_IDIV2,
  I_IDIV4,
  I_IDIV8,
  I_UDIV1,
  I_UDIV2,
  I_UDIV4,
  I_UDIV8,
  I_ADD1,
  I_ADD2,
  I_ADD4,
  I_ADD8,
  I_SUB1,
  I_SUB2,
  I_SUB4,
  I_SUB8,
  I_MOD1,
  I_MOD2,
  I_MOD4,
  I_MOD8,
  I_B_AND1,
  I_B_AND2,
  I_B_AND4,
  I_B_AND8,
  I_B_OR1,
  I_B_OR2,
  I_B_OR4,
  I_B_OR8,
  I_B_XOR1,
  I_B_XOR2,
  I_B_XOR4,
  I_B_XOR8,
  I_B_NOT1,
  I_B_NOT2,
  I_B_NOT4,
  I_B_NOT8,
  I_JMP,
  I_DEREF,
  I_F_ADD,
  I_F_SUB,
  I_F_MUL,
  I_F_DIV,
  I_F_MOD,
  I_F_ABS,
  I_F_POW,
  I_F_FROMI1,
  I_F_FROMI2,
  I_F_FROMI4,
  I_F_FROMI8,
  I_F_IFP,
};

#endif
