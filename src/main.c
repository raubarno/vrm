// Copyright (c) 2023 Arnoldas Rauba
//
//    This program is free software: you can redistribute it
//    and/or modify it under the terms of the
//    GNU General Public License as published by the
//    Free Software Foundation, either version 3 of the License,
//    or (at your option) any later version.
//
//    This program is distributed in the hope that it will be
//    useful, but WITHOUT ANY WARRANTY; without even the implied
//    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//    PURPOSE.
//
//    See the GNU General Public License for more details.
//    You should have received a copy of the
//    GNU General Public License along with this program.
//    If not, see <http://www.gnu.org/licenses/>.

#include "bytevec.h"
#include "lang.h"
#include "ltokens.h"
#include "slice.h"
#include <memory.h>
#include <regex.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

enum block_effect_type_t {
  BET_NONE,
  BET_INSTRUCTION,
  BET_STRUCTDEF,
  BET_VARDEF,
  BET_INSTRUCTIONDEF,
  BET_SETC,
};

enum datatype_t {
  DT_BASIC,
  DT_STRUCT,
};

enum dataclass_t {
  DC_UNSIGNED,
  DC_SIGNED,
  DC_FLOAT,
};

struct basic_type_t {
  const char *name;
  enum dataclass_t dtype;
  size_t size;
};

struct basic_type_t LANG_BASIC_TYPES[] = {
    {"u8", DC_UNSIGNED, 1},  {"u16", DC_UNSIGNED, 2}, {"u32", DC_UNSIGNED, 4},
    {"u64", DC_UNSIGNED, 8}, {"i8", DC_SIGNED, 1},    {"i16", DC_SIGNED, 2},
    {"i32", DC_SIGNED, 4},   {"i64", DC_SIGNED, 8},   {"ptr", DC_UNSIGNED, 1},
    {"f32", DC_FLOAT, 4}};

union value_type_u {
  u8 u_8;
  i8 i_8;
  u16 u_16;
  i16 i_16;
  u32 u_32;
  i32 i_32;
  u64 u_64;
  i64 i_64;
  f32 f_32;
};

struct setc_t {
  size_t context_idx;
  const struct basic_type_t *basic_type;
  union value_type_u value;
};

struct vardef_t {
  enum datatype_t datatype;
  union {
    size_t struct_context_idx;
    const struct basic_type_t *basic_type;
  } data;
  struct bytevec_t name;
};

void vardef_drop(struct vardef_t self) { bytevec_drop(self.name); }

struct instruction_t {
  enum ins_t ins;
  ptr lhs;
  ptr rhs;
};

struct structdef_t {
  struct bytevec_t name;
  // of type (struct vardef_t)
  struct bytevec_t members;
};

struct vardef_t *structdef_get_member(struct structdef_t *self, size_t idx) {
  return &((struct vardef_t *)self->members.data)[idx];
}

size_t structdef_get_members_len(struct structdef_t *self) {
  return self->members.len / sizeof(struct vardef_t);
}

void structdef_drop(struct structdef_t self) {
  size_t len = structdef_get_members_len(&self);
  for (size_t i = 0; i < len; i++) {
    struct vardef_t *vardef = structdef_get_member(&self, i);
    vardef_drop(*vardef);
  }
  bytevec_drop(self.name);
  bytevec_drop(self.members);
}

struct gotodef_t {
  struct bytevec_t name;
  ptr ip;
};

void gotodef_drop(struct gotodef_t self) { bytevec_drop(self.name); }

struct context_t {
  // of type (struct structdef_t)
  struct bytevec_t structdefs;
  // of type (struct vardef_t)
  struct bytevec_t vardefs;
  // of type (struct gotodef_t)
  struct bytevec_t gotodefs;
  ptr ip;
  ptr sp;
};

struct context_t context_new() {
  return (struct context_t){
      .structdefs = bytevec_new(32 * sizeof(struct structdef_t)),
      .vardefs = bytevec_new(32 * sizeof(struct vardef_t)),
      .gotodefs = bytevec_new(32 * sizeof(struct gotodef_t))};
}

struct structdef_t *context_get_structdef(struct context_t *self, size_t idx) {
  return &((struct structdef_t *)self->structdefs.data)[idx];
}

size_t context_get_structdefs_len(struct context_t *self) {
  return self->structdefs.len / sizeof(struct structdef_t);
}

struct vardef_t *context_get_vardef(struct context_t *self, size_t idx) {
  return &((struct vardef_t *)self->vardefs.data)[idx];
}

size_t context_get_vardefs_len(struct context_t *self) {
  return self->vardefs.len / sizeof(struct vardef_t);
}

struct gotodef_t *context_get_gotodef(struct context_t *self, size_t idx) {
  return &((struct gotodef_t *)self->gotodefs.data)[idx];
}

size_t context_get_gotodefs_len(struct context_t *self) {
  return self->gotodefs.len / sizeof(struct gotodef_t);
}

void context_drop(struct context_t self) {
  size_t len = context_get_structdefs_len(&self);
  for (size_t i = 0; i < len; i++) {
    struct structdef_t *structdef = context_get_structdef(&self, i);
    structdef_drop(*structdef);
  }
  len = context_get_vardefs_len(&self);
  for (size_t i = 0; i < len; i++) {
    struct vardef_t *vardef = context_get_vardef(&self, i);
    vardef_drop(*vardef);
  }
  len = context_get_gotodefs_len(&self);
  for (size_t i = 0; i < len; i++) {
    struct gotodef_t *gotodef = context_get_gotodef(&self, i);
    gotodef_drop(*gotodef);
  }
  bytevec_drop(self.structdefs);
  bytevec_drop(self.vardefs);
  bytevec_drop(self.gotodefs);
}

struct block_effect_t {
  enum block_effect_type_t type;
  union {
    struct instruction_t instruction;
    struct structdef_t structdef;
    struct vardef_t vardef;
    struct gotodef_t gotodef;
    struct setc_t setc;
  } data;
};

void block_effect_drop(struct block_effect_t self) {
  switch (self.type) {
  case BET_NONE:
  case BET_INSTRUCTION:
  case BET_SETC:
    break;
  case BET_INSTRUCTIONDEF:
    gotodef_drop(self.data.gotodef);
    break;
  case BET_STRUCTDEF:
    structdef_drop(self.data.structdef);
    break;
  case BET_VARDEF:
    vardef_drop(self.data.vardef);
    break;
  }
}

enum parse_result_type_t {
  PRT_NOT_ENOUGH_DATA,
  PRT_SYNTAX_ERROR,
  PRT_OK,
};

struct parse_result_t {
  enum parse_result_type_t type;
  struct block_effect_t block_effect;
  struct byteslice_t slice;
};

void parse_result_drop(struct parse_result_t self) {
  if (self.type == PRT_OK) {
    block_effect_drop(self.block_effect);
  }
}

struct parse_result_t parse_endline(struct byteslice_t slice,
                                    struct parse_result_t pr) {
  regex_t regex_endline;
  regmatch_t pmatch[1];
  char *begin = (char *)slice.data;
  if (regcomp(&regex_endline, "^[ |\t]*\n", REG_EXTENDED)) {
    abort();
  }
  if (!regexec(&regex_endline, begin, 1, pmatch, 0)) {
    pr.slice.len += pmatch[0].rm_eo;
    return pr;
  }
  parse_result_drop(pr);
  regfree(&regex_endline);
  for (size_t i = 0; i < slice.len; i++) {
    if (slice.data[i] == '\n') {
      return (struct parse_result_t){.type = PRT_SYNTAX_ERROR};
    }
  }

  return (struct parse_result_t){.type = PRT_NOT_ENOUGH_DATA};
}

struct parse_value_result_t {
  bool is_some;
  const struct basic_type_t *type;
  union value_type_u value;
};

struct parse_value_result_t parse_value(struct byteslice_t slice) {
  if (slice.len == 0) {
    return (struct parse_value_result_t){.is_some = false};
  }
  regex_t regex_datatype;
  regmatch_t pmatch[1];
  char *begin = (char *)slice.data;
  struct bytevec_t word = bytevec_new(slice.len);
  if (regcomp(&regex_datatype, "(([u|i](8|16|32|64))|(ptr))", REG_EXTENDED)) {
    abort();
  }
  if (regexec(&regex_datatype, begin, 1, pmatch, 0)) {
    abort();
  }
  bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);
  struct parse_value_result_t result = {.is_some = false, .type = NULL};

  size_t n = sizeof(LANG_BASIC_TYPES) / sizeof(LANG_BASIC_TYPES[0]);
  for (size_t i = 0; i < n; i++) {
    if (strcmp(LANG_BASIC_TYPES[i].name, (char *)word.data) == 0) {
      result.type = &LANG_BASIC_TYPES[i];
      printf("Type %s\n", LANG_BASIC_TYPES[i].name);
      break;
    }
  }
  if (result.type != NULL) {
    if (result.type->dtype == DC_FLOAT) {
      // unsupported yet
      abort();
    }
    begin += pmatch[0].rm_eo + 1;
    size_t vlen = slice.len - (pmatch[0].rm_eo + 1);
    if (begin[0] == '0') {
      // hex or zero
      if (vlen > 1 && begin[1] == 'x') {
        // hex
        vlen -= 2;
        begin += 2;
        if (vlen > result.type->size * 2) {
          printf("Overflow!\nvlen = %lu, type->size*2 = %lu\n", vlen,
                 result.type->size * 2);
          printf("string = '%s'", begin);
        } else {
          // valid hex
          u64 rval = 0;
          for (size_t i = 0; i < vlen; i++) {
            rval *= 16;
            if (begin[i] >= '0' && begin[i] <= '9') {
              rval += begin[i] - '0';
            } else if ((begin[i] >= 'a' && begin[i] <= 'f')) {
              rval += 10 + begin[i] - 'a';
            } else if ((begin[i] >= 'A' && begin[i] <= 'F')) {
              rval += 10 + begin[i] - 'A';
            }
          }
          result.is_some = true;
          result.value.u_64 = rval;
        }
      } else {
        // zero
        result.is_some = true;
        result.value.u_8 = 0;
      }
    } else {
      // decimal
      u64 limit_max = (u64)(-1) >> ((8 - result.type->size) * 8);
      u64 limit = limit_max / 10;
      printf("type size: %lu\n", result.type->size);
      printf("limit: %Ld\n", limit);
      u64 rval = 0;
      bool negative_sign = false;
      if (begin[0] == '-') {
        negative_sign = true;
        vlen--;
        begin++;
      }
      bool is_ok = true;
      for (size_t i = 0; i < vlen; i++) {
        if (rval > limit) {
          // cannot multiply by 10 anymore
          printf("Overflow!\n");
          is_ok = false;
          break;
        }
        rval *= 10;
        u64 diff = limit_max - rval;
        u64 digit = begin[i] - '0';
        if (diff < digit) {
          printf("Overflow!\n");
          is_ok = false;
          break;
        }
        rval += digit;
      }
      if (is_ok) {
        u64 sign_limit = (limit_max / 2);
        if (result.type->dtype == DC_SIGNED &&
            ((negative_sign && rval > sign_limit + 1) ||
             (!negative_sign && rval > sign_limit))) {
          printf("Overflow of a signed integer!\n");
          is_ok = false;
        }
      }
      if (is_ok) {
        result.is_some = true;
        // This is overkill for the love of big endian that nobody uses anyway.
        if (result.type->dtype == DC_SIGNED) {
          switch (result.type->size) {
          case 1:
            result.value.i_8 = (negative_sign) ? -(i8)rval : (i8)rval;
            break;
          case 2:
            result.value.i_16 = (negative_sign) ? -(i16)rval : (i16)rval;
            break;
          case 4:
            result.value.i_32 = (negative_sign) ? -(i32)rval : (i32)rval;
            break;
          case 8:
            result.value.i_64 = (negative_sign) ? -(i64)rval : (i64)rval;
            break;
          }
        } else if (result.type->dtype == DC_UNSIGNED) {
          switch (result.type->size) {
          case 1:
            result.value.u_8 = (u8)rval;
            break;
          case 2:
            result.value.u_16 = (u16)rval;
            break;
          case 4:
            result.value.u_32 = (u32)rval;
            break;
          case 8:
            result.value.u_64 = (u64)rval;
            break;
          }
        }
      }
    }
  }

  bytevec_drop(word);
  regfree(&regex_datatype);
  return result;
}

void print_var(const char *name, const struct basic_type_t *type,
               union value_type_u *value) {
  printf("%s : %s = ", name, type->name);
  if (type->dtype == DC_SIGNED) {
    switch (type->size) {
    case 1:
      printf("%1$d (0x%1$02x)\n", value->i_8);
      break;
    case 2:
      printf("%1$d (0x%1$04x)\n", value->i_16);
      break;
    case 4:
      printf("%1$d (0x%1$08x)\n", value->i_32);
      break;
    case 8:
      printf("%1$Ld (0x%1$016Lx)\n", value->i_64);
      break;
    }
  } else if (type->dtype == DC_UNSIGNED) {
    switch (type->size) {
    case 1:
      printf("%1$d (0x%1$02x)\n", value->u_8);
      break;
    case 2:
      printf("%1$d (0x%1$04x)\n", value->u_16);
      break;
    case 4:
      printf("%1$d (0x%1$08x)\n", value->u_32);
      break;
    case 8:
      printf("%1$Ld (0x%1$016Lx)\n", value->u_64);
      break;
    }
  } else {
    printf("%f\n", value->f_32);
  }
}

struct parse_result_t parse_block(struct byteslice_t *slice,
                                  struct context_t *context) {
  if (slice->len == 0) {
    return (struct parse_result_t){PRT_NOT_ENOUGH_DATA};
  }
  regex_t regex_ws;
  regex_t regex_head;
  regex_t regex_var;
  regex_t regex_typeval;
  regex_t regex_datatype;
  regex_t regex_name;
  regmatch_t pmatch[1];
  char *begin = (char *)slice->data;

  if (regcomp(&regex_ws, "^[ |\t]+", REG_EXTENDED)) {
    abort();
  }
  if (regcomp(&regex_head, "^(!|([a-z|_][a-z|_|0-9]*:?))", REG_EXTENDED)) {
    abort();
  }
#define REGEX_NAME "([a-z|_][a-z|_|0-9]*)"
#define REGEX_HEX "(0(x[0-9|a-f|A-F]+)?)"
#define REGEX_ADDR "(([1-9][0-9]*)|" REGEX_HEX ")"
#define REGEX_TYPEVAL_I "(i(8|16|32|64):((-?[1-9][0-9]*)|" REGEX_HEX "))"
#define REGEX_TYPEVAL_U "(((u(8|16|32|64))|(ptr)):" REGEX_ADDR ")"
  if (regcomp(&regex_var,
              "^((" REGEX_NAME "(\\." REGEX_NAME ")*)|" REGEX_ADDR ")",
              REG_EXTENDED)) {
    abort();
  }
  if (regcomp(&regex_typeval, "^(" REGEX_TYPEVAL_I "|" REGEX_TYPEVAL_U ")",
              REG_EXTENDED)) {
    abort();
  }
  if (regcomp(&regex_datatype, "(([u|i](8|16|32|64))|(ptr))", REG_EXTENDED)) {
    abort();
  }
  if (regcomp(&regex_name, REGEX_NAME, REG_EXTENDED)) {
    abort();
  }

  if (!regexec(&regex_ws, begin, 1, pmatch, 0)) {
    begin = &begin[pmatch[0].rm_eo];
  }
  struct parse_result_t result = {.type = PRT_SYNTAX_ERROR};
  struct bytevec_t word = bytevec_new(32);
  if (!regexec(&regex_head, begin, 1, pmatch, 0)) {
    bytevec_clear(&word);
    bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);
    begin = &begin[pmatch[0].rm_eo];
    printf("Match: '%.*s'\n", (int)word.len, word.data);
    // wlen > 0
    if (word.data[word.len - 1] == ':') {
      word.data[word.len - 1] = '\0';
      word.len--;
      size_t n = context_get_gotodefs_len(context);
      for (size_t i = 0; i < n; i++) {
        char *gotoname = (char *)context_get_gotodef(context, i)->name.data;
        if (strcmp(gotoname, (char *)word.data)) {
          puts("Goto definition already exists!");
          goto finalize;
        }
      }
      result = (struct parse_result_t){
          .type = PRT_OK,
          .block_effect = {.type = BET_INSTRUCTIONDEF,
                           .data.gotodef = {.ip = context->ip, .name = word}}};
      goto finalize2;
    }

    size_t n = sizeof(LANG_TOKENS) / sizeof(LANG_TOKENS[0]);
    size_t f = n;
    for (size_t i = 0; i < n; i++) {
      if (strcmp((char *)word.data, LANG_TOKENS[i].name) == 0) {
        f = i;
        break;
      }
    }
    if (f != n) {
      const size_t STRUCT_MAGIC = 100;
      const struct lang_token_t *ltoken = &LANG_TOKENS[f];
      printf("Found token: '%s'\n", ltoken->name);
      if (!regexec(&regex_ws, begin, 1, pmatch, 0)) {
        begin = &begin[pmatch[0].rm_eo];
      } else {
        goto finalize;
      }
      switch (ltoken->ltt) {
      case LTT_SETC:
      case LTT_SETC1:
      case LTT_SETC2:
      case LTT_SETC4:
      case LTT_SETC8:
        if (!regexec(&regex_var, begin, 1, pmatch, 0)) {
          bytevec_clear(&word);
          bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);
          printf("Var: '%.*s'\n", (int)word.len, word.data);
          begin = &begin[pmatch[0].rm_eo];
        } else {
          break;
        }
        if (!regexec(&regex_ws, begin, 1, pmatch, 0)) {
          begin = &begin[pmatch[0].rm_eo];
        } else {
          break;
        }
        n = context_get_vardefs_len(context);
        size_t addr = n;
        for (size_t i = 0; i < n; i++) {
          char *varname = (char *)context_get_vardef(context, i)->name.data;
          if (strcmp(varname, (char *)word.data) == 0) {
            addr = i;
          }
        }
        if (addr != n) {
          struct byteslice_t sname = bytevec_as_slice(&word, 0, word.len);
          struct bytevec_t newvar_name = bytevec_clone_w0(&sname);
          if (!regexec(&regex_typeval, begin, 1, pmatch, 0)) {
            bytevec_clear(&word);
            bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);
            printf("Typeval: '%.*s'\n", (int)word.len, word.data);
            struct parse_value_result_t valres =
                parse_value(bytevec_as_slice(&word, 0, word.len));
            if (valres.is_some) {
              print_var((char *)newvar_name.data, valres.type, &valres.value);
              result.type = PRT_OK;
              result.block_effect =
                  (struct block_effect_t){.type = BET_SETC,
                                          .data.setc = {
                                              .context_idx = addr,
                                              .basic_type = valres.type,
                                              .value = valres.value,
                                          }};
            }
            begin = &begin[pmatch[0].rm_eo];
          }
          bytevec_drop(newvar_name);
        } else {
          printf("setc: Variable '%s' does not exist.\n", (char *)word.data);
        }
        break;
      case LTT_VARDEF:
        if (regexec(&regex_name, begin, 1, pmatch, 0)) {
          printf("let: Expected data type.\n");
          goto finalize;
        }
        bytevec_clear(&word);
        bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);
        begin += pmatch[0].rm_eo;

        n = sizeof(LANG_BASIC_TYPES) / sizeof(LANG_BASIC_TYPES[0]);
        f = n;
        for (size_t i = 0; i < n; i++) {
          if (strcmp(LANG_BASIC_TYPES[i].name, (char *)word.data) == 0) {
            f = i;
            printf("Type %s\n", LANG_BASIC_TYPES[i].name);
            break;
          }
        }
        if (f == n) {
          n = context_get_structdefs_len(context);
          for (size_t i = 0; i < n; i++) {
            char *structname =
                (char *)context_get_structdef(context, i)->name.data;
            if (strcmp(structname, (char *)word.data) == 0) {
              f = STRUCT_MAGIC + i;
              break;
            }
          }
        }
        n = sizeof(LANG_BASIC_TYPES) / sizeof(LANG_BASIC_TYPES[0]);
        if (f == n) {
          printf("let: Data type '%s' does not exist\n", word.data);
          goto finalize;
        }
        if (regexec(&regex_name, begin, 1, pmatch, 0)) {
          goto finalize;
        }
        bytevec_clear(&word);
        bytevec_push_back_w0(&word, (u8 *)begin, pmatch[0].rm_eo);

        n = context_get_vardefs_len(context);
        for (size_t i = 0; i < n; i++) {
          char *varname = (char *)context_get_vardef(context, i)->name.data;
          if (strcmp(varname, (char *)word.data) == 0) {
            goto finalize;
          }
        }

        result.type = PRT_OK;
        result.block_effect = (struct block_effect_t){
            .type = BET_VARDEF, .data.vardef = {.name = word}};
        if (f < STRUCT_MAGIC) {
          result.block_effect.data.vardef.datatype = DT_BASIC;
          result.block_effect.data.vardef.data.basic_type =
              &LANG_BASIC_TYPES[f];
        } else {
          result.block_effect.data.vardef.datatype = DT_STRUCT;
          result.block_effect.data.vardef.data.struct_context_idx =
              f - STRUCT_MAGIC;
        }
        begin += pmatch[0].rm_eo;
        goto finalize2;
      case LTT_OP_I:
      case LTT_OP_V:
      case LTT_OP_VI:
      case LTT_OP_VV:
      case LTT_STRUCTDEF:
      case LTT_WOP_V:
      case LTT_WOP_VI:
      case LTT_WOP_VV:
        break;
      }
      goto finalize;
    }
  } else {
    result.type = PRT_OK;
    result.block_effect = (struct block_effect_t){.type = BET_NONE};
  }

finalize:
  bytevec_drop(word);
finalize2:
  result.slice.len = (u8 *)begin - slice->data;
  regfree(&regex_ws);
  regfree(&regex_head);
  regfree(&regex_var);
  regfree(&regex_typeval);
  regfree(&regex_datatype);
  regfree(&regex_name);
  struct byteslice_t endslc = {.data = (u8 *)begin,
                               .len = &slice->data[slice->len] - (u8 *)begin};
  return parse_endline(endslc, result);
}

int main() {
  const size_t SZ_PACKET = 32;
  FILE *fd = stdin;
  struct bytevec_t streambuf = bytevec_new(SZ_PACKET);
  u8 stream_packet[SZ_PACKET];
  size_t transmitted = 0;
  struct context_t context = context_new();
  while ((transmitted = read(fileno(fd), stream_packet, SZ_PACKET)) > 0) {
    bytevec_push_back_w0(&streambuf, stream_packet, transmitted);
    for (;;) {
      struct byteslice_t slc = bytevec_as_slice(&streambuf, 0, streambuf.len);
      struct parse_result_t pr = parse_block(&slc, &context);
      if (pr.type == PRT_OK) {
        puts("OK");
        // fwrite(streambuf.data, 1, pr.slice.len, stdout);
        bytevec_pop_front(&streambuf, pr.slice.len);
      } else if (pr.type == PRT_SYNTAX_ERROR) {
        puts("Syntax Error");
        bytevec_clear(&streambuf);
      } else {
        break;
      }
    }
  }
  context_drop(context);

  if (transmitted == 0) {
    fflush(stdout);
    bytevec_drop(streambuf);
    fwrite("EOF\n", 1, 4, stderr);
    return 0;
  } else if (transmitted == -1) {
    perror("I/O error");
    bytevec_drop(streambuf);
    return 1;
  }

  return 0;
}
