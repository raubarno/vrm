// Copyright (c) 2023 Arnoldas Rauba
//
//    This program is free software: you can redistribute it
//    and/or modify it under the terms of the
//    GNU General Public License as published by the
//    Free Software Foundation, either version 3 of the License,
//    or (at your option) any later version.
//
//    This program is distributed in the hope that it will be
//    useful, but WITHOUT ANY WARRANTY; without even the implied
//    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//    PURPOSE.
//
//    See the GNU General Public License for more details.
//    You should have received a copy of the
//    GNU General Public License along with this program.
//    If not, see <http://www.gnu.org/licenses/>.

#include "bytevec.h"
#include "lang.h"
#include "slice.h"
#include <memory.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct bytevec_t bytevec_new(size_t len) {
  u8 *data = malloc(len);
  if (data == NULL) {
    perror("malloc: ");
    abort();
  }
  struct bytevec_t r = {0, len, data};
  return r;
}

void bytevec_push_back(struct bytevec_t *self, u8 *data, size_t len) {
  if (self->len + len > self->cap) {
    u8 *newbuf = malloc(self->cap * 2);
    if (newbuf == NULL) {
      perror("malloc: ");
      abort();
    }
    memcpy(newbuf, self->data, self->len);
    free(self->data);
    self->data = newbuf;
    self->cap *= 2;
  }
  memcpy(&self->data[self->len], data, len);
  self->len += len;
}

void bytevec_push_back_w0(struct bytevec_t *self, u8 *data, size_t len) {
  if (self->len + len + 1 > self->cap) {
    u8 *newbuf = malloc(self->cap * 2);
    if (newbuf == NULL) {
      perror("malloc: ");
      abort();
    }
    memcpy(newbuf, self->data, self->len);
    free(self->data);
    self->data = newbuf;
    self->cap *= 2;
  }
  memcpy(&self->data[self->len], data, len);
  self->len += len;
  self->data[self->len] = 0;
}

void bytevec_pop_front(struct bytevec_t *self, size_t len) {
  memmove(self->data, &self->data[len], self->len - len);
  self->len -= len;
}

struct byteslice_t bytevec_as_slice(struct bytevec_t *self, size_t from,
                                    size_t len) {
  if (from + len > self->len) {
    const char *errmsg = "Cannot assign slice length beyond array length";
    fwrite(errmsg, 1, strlen(errmsg), stderr);
    abort();
  }
  struct byteslice_t r = {&self->data[from], len};
  return r;
}

struct bytevec_t bytevec_clone_w0(struct byteslice_t *from) {
  size_t newsz = from->len + 1;
  if (newsz < 32) {
    newsz = 32;
  }
  struct bytevec_t result = bytevec_new(newsz);
  bytevec_push_back_w0(&result, from->data, from->len);
  return result;
}

struct bytevec_t bytevec_clone(struct byteslice_t *from) {
  size_t newsz = from->len;
  if (newsz < 32) {
    newsz = 32;
  }
  struct bytevec_t result = bytevec_new(newsz);
  bytevec_push_back(&result, from->data, from->len);
  return result;
}

void bytevec_clear(struct bytevec_t *self) { self->len = 0; }

void bytevec_drop(struct bytevec_t self) { free(self.data); }
