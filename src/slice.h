// Copyright (c) 2023 Arnoldas Rauba
//
//    This program is free software: you can redistribute it
//    and/or modify it under the terms of the
//    GNU General Public License as published by the
//    Free Software Foundation, either version 3 of the License,
//    or (at your option) any later version.
//
//    This program is distributed in the hope that it will be
//    useful, but WITHOUT ANY WARRANTY; without even the implied
//    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//    PURPOSE.
//
//    See the GNU General Public License for more details.
//    You should have received a copy of the
//    GNU General Public License along with this program.
//    If not, see <http://www.gnu.org/licenses/>.

#ifndef H_SLICE
#define H_SLICE
#include "lang.h"
#include <stddef.h>

struct byteslice_t {
  u8 *data;
  size_t len;
};

#endif
